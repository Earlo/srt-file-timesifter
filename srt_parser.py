import pygame
import os
from pygame.locals import *


import menu




class game():
	
	def __init__(self):
		
		self.FPS = 30 # 60 frames per second			
		self.SWIDTH =  800
		self.SHEIGTH = 600	
		self.MainWindow = pygame.display.set_mode((self.SWIDTH, self.SHEIGTH))
		self.selected_file = None
		self.active_text_field = None
		
		self.mouse = [pygame.mouse.get_pos() , False ,  [0,0] , None ]

		
		self.surf_GUI = pygame.Surface((self.SWIDTH, self.SHEIGTH))
		self.updates = []
		self.erase = []
		self.draw = []
		
		#path = os.path.join("recourses","cursor.bmp")
		
		self.funktion = None
		self.GUI = []
		self.done = False
		self.debug_message = None
		pygame.display.set_caption("srt time sifter")
	def m_loop(self):
		while not self.done:
			if not self.debug_message == None:
				print self.debug_message
				self.debug_message  = None
			self.keys = []
			self.mouse[0] = pygame.mouse.get_pos()
			self.mouse[1] = False
			for event in pygame.event.get(): # User did something
				if event.type == pygame.QUIT: # Closed from X
					self.done = True # Stop the Loop
				if event.type == pygame.MOUSEBUTTONUP:
					self.mouse[1] = [True]
					for object in self.GUI:#check if any in game buttons are pressed
						if object.pressed(self.mouse[0]):
							object.act()
				if event.type == pygame.KEYDOWN:
					self.keys.append(event.key)
					
			if len(self.keys) > 0 and not self.active_text_field == None:
				self.active_text_field.update()
			self.funktion()
						
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~File select menu setup and loop~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~			
	def sfile_menu(self):
		self.GUI = []
		self.GUI.append(menu.button(pygame.Rect(self.MainWindow.get_size()[0]-180,self.MainWindow.get_size()[1]-100,160,80),"Change offset",[self.sedit_file, "ONETIME"]))
		self.seconds = menu.input_box(pygame.Rect(self.MainWindow.get_size()[0]-180,self.MainWindow.get_size()[1]-200,80,20),"Seconds")
		self.minutes = menu.input_box(pygame.Rect(self.MainWindow.get_size()[0]-180,self.MainWindow.get_size()[1]-250,80,20),"Minutes")
		self.GUI.append(self.seconds)
		self.GUI.append(self.minutes)
		x = 0
		y = 0
		for file in os.listdir('Subs'):
			suffix = file.split(".")[-1]
			if suffix == "srt":
				name = file.split(".")[0]
				self.GUI.append(menu.button(pygame.Rect(10+100*x,10+100*y,80,60),name,[self.change_file, name, "ONETIME"]))
				x += 1
				if (x-1)*100 == self.SWIDTH:
					x  = 0
					y += 1
				
			
		self.MainWindow.fill((100,100,100))
		
		self.refresh_GUI()
		
		pygame.display.flip()
		self.funktion = self.file_menu		
		
	def file_menu(self):
		pass
		
	def change_file(self, new):
		new = new
		if self.selected_file == None:
			self.selected_file = new		
			print "set",new,"as selected file \n" 
		elif not self.selected_file == new:
			old = self.selected_file
			self.selected_file = new		
			print "selected file changed to",new,"from",old,"\n" 
		else:
			print "selected file already is",new,"\n" 	
	
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Actual parser and editor~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	def sedit_file(self):
		if self.selected_file == None:
			print "Please select a file to sift first"
		else:
			fail = True
			self.offset = 0
			try:
				self.offset += int(self.seconds.string)
				fail = False
			except ValueError:
				pass
			try:
				self.offset += int(self.minutes.string)*60
				fail = False
			except ValueError:
				pass		
			if fail:
				print "Please set some kind of offset for the file"
			else:
				print "moving all lines by",self.offset,"seconds"
				self.funktion = self.edit_file
				
	def edit_file(self):
		path = os.path.join("Subs", self.selected_file + ".srt")
		end_path = os.path.join("Subs", "Sifted_Subs", self.selected_file +"_sifted_by_"+str(self.offset)+"_seconds"+ ".srt")
		file = open(path)
		result = ""
		for line in file:
			parts = line.split("-->")
			if len(parts) == 2:
				new = []
				for part in parts:
					hours,minutes,seconds = part.split(":")
					hours = int(hours)
					minutes = int(minutes)
					s_p = seconds.split(",")
					seconds = float(s_p[0]+"."+s_p[1])
					seconds += self.offset
					while seconds > 60:
						minutes += 1
						seconds -= 60
					while seconds < 0:
						minutes -= 1
						seconds += 60
					while minutes > 60:
						hours += 1
						minutes -= 60
					while minutes < 0:
						hours -= 1
						minutes += 60
					hours = str(hours)
					minutes = str(minutes)
					seconds = str(seconds)
					if len(hours) == 1:
						hours = "0"+hours
					if len(minutes) == 1:
						minutes = "0"+minutes	
					s_p = seconds.split(".")
					b = "00"
					s_p[0] = b[len(s_p[0])-1:1]+s_p[0]
					b = "000"
					s_p[1] = b[len(s_p[1])-1:2]+s_p[1]
					seconds = s_p[0]+","+s_p[1]
					part = str(hours)+":"+str(minutes)+":"+str(seconds)
					new.append(part)
				sifted = new[0]+" --> "+new[1]+"\n"
			else:
				sifted = line
			result += sifted
		result_file = open(end_path, 'w+')
		result_file.write(result)
		print "DONE!"
		self.funktion = self.sfile_menu
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Game setup and loop~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	
	def run_command(self, commands):
		for c in commands:
			command = c[:]
			if command[0] == "ONETIME":
				command.pop(0)
				f = command[0]
				command.pop(0)
				f(*command)
			else:
				self.funktion = command[0]
				
	def refresh_GUI(self):
		for object in self.GUI:
			object.draw()
			
	def add_to_(self,object,list):
		list.append(object)

def start():
	global PROGRAM
	PROGRAM = game()
	PROGRAM.funktion = PROGRAM.sfile_menu
	PROGRAM.m_loop()
	pygame.quit()		