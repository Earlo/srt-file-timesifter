# coding: utf-8
#This is the main file. Run this, to run the program

import pygame, sys
import srt_parser

#Setup
pygame.init()
pygame.font.init()
	
if __name__ == "__main__":
	srt_parser.start()
	sys.exit()